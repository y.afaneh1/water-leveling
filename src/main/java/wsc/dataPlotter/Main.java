/**********created on Wednesday May 26, 2021

@Author: Yousef Afaneh****************/

package wsc.dataPlotter;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.knowm.xchart.*;

import javax.swing.*;
import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.colors.ChartColor;
import org.knowm.xchart.style.colors.XChartSeriesColors;
import org.knowm.xchart.style.lines.SeriesLines;
import org.knowm.xchart.style.markers.SeriesMarkers;


public class Main {
    public static void main(String[] args) throws ParseException {

        ArrayList<String[]> listofStationsInfo = getStationsInfo(1);
        String typeOfData = "";
        String parseTypeOfData = "";
        String yAxisTitle = "";
        Object[] options = getStationsInfo(2).toArray(); // select station number field
        String[] dataType = {"Stage", "Discharge"};

        //Request from the user to select a station number
        String selectedStation = (String) JOptionPane.showInputDialog(null,
                "Choose a station you want to display from the dropdown menu below",
                "Station Selection", JOptionPane.QUESTION_MESSAGE, null,
                options, // List of station numbers
                getStationsInfo(2).get(0)); // Select first station Id on the list as default

        //Identify whether the selected station is Stage Only, Discharge Only, or both Stage and Discharge
        for (int i = 0; i < listofStationsInfo.size(); i++) {
            String[] stationInfo = listofStationsInfo.get(i);
            if (selectedStation.equals(stationInfo[0])) {
                if (stationInfo[3].contains("Stage and Discharge")) { // Ask user to select data type (Stage/Discharge)
                    typeOfData = (String) JOptionPane.showInputDialog(null,
                            "Choose the type of data you want to display",
                            "Data Type Selection", JOptionPane.QUESTION_MESSAGE, null, // Use
                            dataType, // Stage or Discharge
                            null);
                    parseTypeOfData = typeOfData.equals("Stage") ? "S" : "D";
                    i = listofStationsInfo.size();
                }
                if (stationInfo[3].contains("Stage Only")) {
                    parseTypeOfData = "S";
                    i = listofStationsInfo.size();
                }
                if (stationInfo[3].contains("Discharge Only")) {
                    parseTypeOfData = "D";
                    i = listofStationsInfo.size();
                }
            }
        }
        // Identify the yAxis title on the graph based on data type
        if (parseTypeOfData == "S") {
            yAxisTitle = "Stage (m)";
        }else{
            yAxisTitle = "Discharge (m^3)/s";
        }

        String stationToLookup = parseTypeOfData + selectedStation;

        //retrieving real time data from getRealTimeData method
        Map<String, HashMap<LocalDateTime, Double>> realTimeData = getRealTimeData();
        HashMap<LocalDateTime, Double> stationDataMap = realTimeData.get(stationToLookup);

        //retrieving  historical data from getHistoricData method
        Map<String, HashMap<LocalDateTime, Double>> historicalTimeData = getHistoricData();
        HashMap<LocalDateTime, Double> historicalStationDataMap = historicalTimeData.get(stationToLookup);

        //Listing all the real datetime data and grouping them by year
        List<Date> realTimeXAxisList = new ArrayList<>();
        Map<String, List<LocalDateTime>> fullDataSet = new HashMap();
        Set<LocalDateTime> realtimeDateTimeDataSet = stationDataMap.keySet();
        Map<Integer, List<LocalDateTime>> yearsList = group(realtimeDateTimeDataSet, ChronoField.YEAR);

        //Grouping the real datetime data by month of each year (There are 3 months overlap for 2021 so 15 months total)
        for(Integer yearNumber : yearsList.keySet()){
            List<LocalDateTime> localDateTimes = yearsList.get(yearNumber);
            Map<Integer, List<LocalDateTime>> monthsList = group(localDateTimes, ChronoField.MONTH_OF_YEAR); //1-12
            for(Integer monthValue : monthsList.keySet()){
                LocalDateTime localDateTime = monthsList.get(monthValue).get(0);
                Date convertedDate = java.sql.Timestamp.valueOf(localDateTime);
                realTimeXAxisList.add(convertedDate);
                String monthStringValue = monthValue >= 10 ? monthValue.toString() : "0"+monthValue.toString();
                fullDataSet.put(yearNumber + "/" + monthStringValue, monthsList.get(monthValue));
            }
        }
        //Listing all the historic datetime data
        List<Date> historicXAxisList = new ArrayList<>();
        Map<String, List<LocalDateTime>> historicFullDataSet = new HashMap();
        Set<LocalDateTime> historicDataSet = historicalStationDataMap.keySet(); //set of all the keys

        //Grouping the historic datetime data by months (12 months)
        Map<Integer, List<LocalDateTime>> hMonthsList = group(historicDataSet, ChronoField.MONTH_OF_YEAR);
        for (Integer monthValue : hMonthsList.keySet()) {
            LocalDateTime localDateTime = hMonthsList.get(monthValue).get(0);
            Date convertedDate = java.sql.Timestamp.valueOf(localDateTime);
            historicXAxisList.add(convertedDate);
            String monthStringValue =
                monthValue >= 10 ? monthValue.toString() : "0" + monthValue.toString();
            historicFullDataSet.put(monthStringValue, hMonthsList.get(monthValue));
        }

        //sort the historical and real time data by ascending order
        TreeMap<String, List<LocalDateTime>> realTimeSortedDataList = sortbykey(fullDataSet);
        TreeMap<String, List<LocalDateTime>> hSortedDataList = sortbykey(historicFullDataSet);

        //Creating monthly averages for the realtime data set (for faster graphing)
        //TODO: Create daily or hourly averages for users who requires more details
        List<Double> yAxisList = new ArrayList<>();
        for(String xAxis : realTimeSortedDataList.keySet()){
            List<LocalDateTime> monthlyData = realTimeSortedDataList.get(xAxis);
            Double totalValue = 0.0;
            for (LocalDateTime md : monthlyData) {
                Double currValue = stationDataMap.get(md);
                totalValue += currValue;
            }
            Double average = totalValue / monthlyData.size();
            yAxisList.add(average);
        }

        List<Double> hYAxisList = new ArrayList<>();
        List<Double> minYAxisList = new ArrayList<>();
        List<Double> maxYAxisList = new ArrayList<>();

        //Getting the historic monthly average and the minimum/maximum values ever recorded
        for(String xAxis : hSortedDataList.keySet()){
            List<LocalDateTime> monthlyData = hSortedDataList.get(xAxis);
            Double totalValue = 0.0;
            Double maxValue = 0.0;
            Double minValue = Double.MAX_VALUE ;
            for (LocalDateTime md : monthlyData) {
                Double currValue = historicalStationDataMap.get(md);
                if(currValue > maxValue ){ maxValue = currValue;}
                if(currValue < minValue){ minValue = currValue;}
                totalValue += currValue;
            }
            minYAxisList.add(minValue);
            maxYAxisList.add(maxValue);

            Double average = totalValue / monthlyData.size();
            hYAxisList.add(average);
        }

        //Aligning the historical data with the realtime data to make both have equal amounts of months
        //This accounts for the fact that we have stations with real time data that extends to the next year
        if(yAxisList.size() != hYAxisList.size()){
            Integer diff =  yAxisList.size() - hYAxisList.size();
            for(int i = 0; i < diff; i++){
                hYAxisList.add(hYAxisList.get(i%12));
                minYAxisList.add(minYAxisList.get(i%12));
                maxYAxisList.add(maxYAxisList.get(i%12));
            }
        }
        // Giving the parameters to the charting method and graphing it
        XYChart chart = getChart(selectedStation, realTimeXAxisList, yAxisList, yAxisTitle, realTimeXAxisList, hYAxisList,realTimeXAxisList, maxYAxisList,realTimeXAxisList, minYAxisList);
        new SwingWrapper<XYChart>(chart).displayChart();

    }

    //Method used for grouping data by year and by month
    private static Map<Integer, List<LocalDateTime>> group(Collection<LocalDateTime> elements, ChronoField chronoField) {

        return elements.stream().collect(Collectors.groupingBy(
            x -> x.get(chronoField)));
    }

    // Charting method
    public static XYChart getChart(String selectedStation, List<Date> xData, List<Double> yData, String yAxisTitle, List<Date> xAvgData, List<Double> yAvgData, List<Date> xMaxData, List<Double> yMaxData, List<Date> xMinData, List<Double> yMinData) {

        // Build chart
        XYChart chart = new XYChartBuilder().width(1600).height(800).title(selectedStation + " - " + yAxisTitle).xAxisTitle("Real Time Date").yAxisTitle(yAxisTitle).build();

        // Customize Chart
        chart.getStyler().setPlotBackgroundColor(ChartColor.getAWTColor(ChartColor.GREY));
        chart.getStyler().setPlotGridLinesColor(new Color(255, 255, 255));
        chart.getStyler().setChartBackgroundColor(Color.WHITE);
        chart.getStyler().setLegendBackgroundColor(Color.WHITE);
        chart.getStyler().setChartFontColor(Color.MAGENTA);
        chart.getStyler().setChartTitleBoxBackgroundColor(new Color(0, 222, 125));
        chart.getStyler().setChartTitleBoxVisible(true);
        chart.getStyler().setChartTitleBoxBorderColor(Color.BLACK);
        chart.getStyler().setPlotGridLinesVisible(false);

        chart.getStyler().setAxisTickPadding(20);

        chart.getStyler().setAxisTickMarkLength(20);

        chart.getStyler().setPlotMargin(20);

        chart.getStyler().setChartTitleFont(new Font(Font.MONOSPACED, Font.BOLD, 24));
        chart.getStyler().setLegendFont(new Font(Font.SERIF, Font.PLAIN, 18));
        chart.getStyler().setLegendPosition(LegendPosition.OutsideE);
        chart.getStyler().setLegendSeriesLineLength(12);
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 18));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SERIF, Font.PLAIN, 11));
        chart.getStyler().setDatePattern("yyyy-MM");
        chart.getStyler().setDecimalPattern("#0.000");
        chart.getStyler().setLocale(Locale.ENGLISH);
        chart.getStyler().setPlotGridHorizontalLinesVisible(true);
        chart.getStyler().setPlotGridVerticalLinesVisible(true);

        // Real time series
        XYSeries series = chart.addSeries("Real Time " + yAxisTitle, xData, yData);
        series.setLineColor(XChartSeriesColors.GREEN);
        series.setMarkerColor(Color.ORANGE);
        series.setMarker(SeriesMarkers.CIRCLE);
        series.setLineStyle(SeriesLines.SOLID);

        // Historical min series
        if(xMinData != null && yMinData != null) {
            XYSeries minSeries = chart.addSeries("Min " + yAxisTitle, xMinData, yMinData);
            minSeries.setLineColor(XChartSeriesColors.BLUE);
            minSeries.setMarkerColor(Color.MAGENTA);
            minSeries.setMarker(SeriesMarkers.TRIANGLE_UP);
            minSeries.setLineStyle(SeriesLines.DASH_DOT);
        }

        // Historical max series
        if(xMaxData != null && yMaxData != null) {
            XYSeries maxSeries = chart.addSeries("Max " + yAxisTitle, xMaxData, yMaxData);
            maxSeries.setLineColor(XChartSeriesColors.RED);
            maxSeries.setMarkerColor(Color.MAGENTA);
            maxSeries.setMarker(SeriesMarkers.TRIANGLE_DOWN);
            maxSeries.setLineStyle(SeriesLines.DASH_DOT);
        }

        // Historical average series
        if(xAvgData != null && yAvgData != null) {
            XYSeries avgSeries = chart.addSeries("Historical Average " + yAxisTitle, xAvgData, yAvgData);
            avgSeries.setLineColor(XChartSeriesColors.ORANGE);
            avgSeries.setMarkerColor(Color.RED);
            avgSeries.setMarker(SeriesMarkers.DIAMOND);
            avgSeries.setLineStyle(SeriesLines.SOLID);
        }
        return chart;
    }

    // getting raw stations info from Stations.csv file
    public static ArrayList<String[]> getStationsInfo(int i) {
        String stationsDir = "src/main/resources/List of Stations/";
        String lineData = "";
        final ArrayList<String[]> stationsInfoList = new ArrayList<>();
        final ArrayList stationsNames = new ArrayList<>();
        try {
            BufferedReader sbr = new BufferedReader(new FileReader(stationsDir + "Stations.csv"));
            sbr.readLine(); //Skips first line
            while ((lineData = sbr.readLine()) != null) {
                String[] stationsData = lineData.split(",");
                stationsInfoList.add(stationsData);
                stationsNames.add(stationsData[0]);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (i == 1) {
            return  stationsInfoList;
        }
        if (i == 2) {
            return stationsNames;
        }
        return null;
    }

    // Getting raw real time data from the given csv files
    public static Map<String, HashMap<LocalDateTime,Double>> getRealTimeData (){
        String realTimeDir = "src/main/resources/Real Time Data/";
        BufferedReader realbr = null;
        String lineData = "";
        Map<String,HashMap<LocalDateTime,Double>> stationMappedData = new HashMap<>();
        Double measuredValue = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        File realTimeData = new File(realTimeDir);
        String[] containingFileNames = realTimeData.list();

        //Going through all the csv files
        for (String fileName : containingFileNames) {
            //Grouping Discharge data
            if (fileName.contains("Flow") && fileName.endsWith(".csv")) {
                HashMap<LocalDateTime,Double> mappedData = new HashMap<>();
                try {
                    realbr = new BufferedReader(new FileReader(realTimeDir + fileName));

                    for(int i=1;i<=5;i++) //Skip first 5 lines as they're not needed
                    {
                        realbr.readLine();
                    }
                    while ((lineData = realbr.readLine()) != null) {
                        if (lineData.length()>1 && !lineData.endsWith(",")) // Some lines in csv files are missing values
                        {
                            String[] values = lineData.split(",");

                                //parsing each datetime and measured value, then adding it to a map
                                LocalDateTime dateTime = LocalDateTime.parse(values[0], formatter);
                                measuredValue = Double.valueOf(values[2]);
                                mappedData.put(dateTime, measuredValue);
                                Arrays.fill(values, null);
                        }else {
                            realbr.readLine(); //Skip line with missing value
                        }
                    }
                    } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Adding real time Discharge data to a map
                stationMappedData.put('D' + StringUtils.substringBefore(fileName,"_"), mappedData);
                }

            //Grouping Stage data
            if (fileName.contains("Level") && fileName.endsWith(".csv")) {
                HashMap<LocalDateTime,Double> mappedData = new HashMap<>();
                try {
                    realbr = new BufferedReader(new FileReader(realTimeDir + fileName));
                    for(int i=1;i<=5;i++) //Skip first 5 lines
                    {
                        realbr.readLine();
                    }

                    while ((lineData = realbr.readLine()) != null) {
                        if (lineData.length()>1 && !lineData.endsWith(",")) // Some lines in csv files are missing values
                        {
                            String[] values = lineData.split(",");

                            //parsing each datetime and measured value, then adding it to a map
                            LocalDateTime dateTime = LocalDateTime.parse(values[0], formatter);
                            measuredValue = Double.valueOf(values[2]);
                            mappedData.put(dateTime, measuredValue);
                            Arrays.fill(values, null);
                        }else {
                            realbr.readLine(); //Skip line with null value
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Adding real time Stage data to a map
                stationMappedData.put('S'+StringUtils.substringBefore(fileName,"_"), mappedData);
            }

            }
          return stationMappedData;
        }

    // Getting raw historic data from the given csv files
    public static Map<String, HashMap<LocalDateTime,Double>> getHistoricData() {
        String historicDir = "src/main/resources/Historic Daily Means/";
        BufferedReader hisbr = null;
        String lineData = "";
        Map<String,HashMap<LocalDateTime,Double>> stationMappedData = new HashMap<>();
        Double measuredValue = null;

        File historicData = new File(historicDir);
        String[] containingFileNames = historicData.list();

        //Going through all the csv files
        for (String fileName : containingFileNames) {
            //Grouping Discharge data
            if (fileName.contains("Flow") && fileName.endsWith(".csv")) {
                HashMap<LocalDateTime, Double> mappedData = new HashMap<>();
                try {
                    hisbr = new BufferedReader(new FileReader(historicDir + fileName));
                    hisbr.readLine(); //Skip first line

                    while ((lineData = hisbr.readLine()) != null) {
                        if (lineData.length() > 1) //Handling lines with missing data
                        {
                            String[] values = lineData.split(",");

                            //parsing each date and measured value, then adding it to a map
                            if(values[1] != null && values[2] != null) {
                                Date datevalue = new SimpleDateFormat("yyyy-MM-dd").parse(values[1]);
                                LocalDateTime dateTime = datevalue.toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDateTime();
                                measuredValue = Double.valueOf(values[2]);
                                mappedData.put(dateTime, measuredValue);
                                Arrays.fill(values, null);
                            }else{
                                hisbr.readLine(); //Skip line with null value
                            }
                        } else {
                            hisbr.readLine(); //Skip null lines
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //Adding historic Discharge data to a map
                stationMappedData.put('D'+StringUtils.substringBefore(fileName,"_"), mappedData);
            }

            //Grouping Stage data
            if (fileName.contains("Level") && fileName.endsWith(".csv")) {
                HashMap<LocalDateTime, Double> mappedData = new HashMap<>();
                try {
                    hisbr = new BufferedReader(new FileReader(historicDir + fileName));
                    hisbr.readLine(); //Skip first line

                    while ((lineData = hisbr.readLine()) != null) {
                        if (lineData.length() > 1) //Handling lines with missing data

                        {
                            String[] values = lineData.split(",");

                            //parsing each date and measured value, then adding it to a map
                            if(values[1] != null && values[2] != null) {
                                Date datevalue = new SimpleDateFormat("yyyy-MM-dd").parse(values[1]);
                                LocalDateTime dateTime = datevalue.toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDateTime();
                                measuredValue = Double.valueOf(values[2]);
                                mappedData.put(dateTime, measuredValue);
                                Arrays.fill(values, null);
                            }else{
                                hisbr.readLine(); //Skip line with null value
                            }
                        } else {
                            hisbr.readLine(); //Skip line with null value
                        }
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //Adding historic Stage data to a map
                stationMappedData.put('S'+StringUtils.substringBefore(fileName,"_"), mappedData);
            }
        }

        return stationMappedData;
    }

    // Function to sort map by Key
    public static TreeMap<String, List<LocalDateTime>> sortbykey(Map<String, List<LocalDateTime>> map)
    {
        // TreeMap to store values of HashMap
        TreeMap<String, List<LocalDateTime>> sorted = new TreeMap<>();

        // Copy all data from hashMap into TreeMap
        sorted.putAll(map);
        return sorted;
    }

    }

